#!/usr/bin/env ruby

require_relative '../lib/simplekit'

# Create a Customer Struct that will store arrival times and processing times.
Customer = Struct.new(:arrival_time, :processing_time) do
  include Comparable
  # rank customers by their processing times, smallest first.
  def <=>(other)
    processing_time <=> other.processing_time
  end
end

# Demonstration model of a shortest-processing-time-first queueing system.
# There are k servers and both the arrival and service processes could
# be anything.
class SPTF
  include SimpleKit

  # Constructor - initializes the model parameters.
  # param: arrivalRate - The rate at which customers arrive to the system.
  # param: serviceRate - The rate at which individual servers serve.
  # param: maxServers - The total number of servers in the system.
  # param: closeTime - The time the server would like to shut down.
  def initialize(arrivalRate, serviceRate, maxServers, closeTime)
    @arrivalRate = arrivalRate
    @serviceRate = serviceRate
    @maxServers = maxServers
    @closeTime = closeTime
  end

  # Initialize the model state and schedule any necessary events.
  def init
    @numAvailableServers = @maxServers
    @q = PriorityQueue.new
    schedule(:arrival, 0.0)
    schedule(:close_doors, @closeTime)
  end


  # An arrival event generates a customer and their associated service
  # time, adds the customer to the queue, schedules the next arrival,
  # and schedules a beginService event if a server is available.
  def arrival
    @q.push Customer.new(model_time, exponential(@serviceRate))
    schedule(:arrival, exponential(@arrivalRate))
    schedule(:beginService, 0.0) if (@numAvailableServers > 0)
  end

  # Start service for the first customer in line, removing that
  # customer from the queue and utilizing one of the available
  # servers.  An endService will be scheduled.  Report the current
  # time and how long this customer spent in line.
  def beginService
    current_customer = @q.pop
    @numAvailableServers -= 1
    schedule(:endService, current_customer.processing_time)
    printf "%f,%f\n", model_time, model_time - current_customer.arrival_time
  end

  # Frees up an available server, and schedules a beginService if
  # anybody is waiting in line.  If the line is empty and it's after
  # the desired closing time, halt the simulation.
  def endService
    @numAvailableServers += 1
    unless @q.empty?
      schedule(:beginService, 0.0)
    end
  end

  # Commence shutdown by denying the next :arrival
  def close_doors
    cancel :arrival
  end

  # Exponential random variate generator.
  # param: rate - The rate (= 1 / mean) of the distribution.
  # returns: A realization of the specified distribution.
  def exponential(rate)
    -Math.log(rand) / rate
  end
end

# Instantiate an SPTF object with a particular parameterization and run it.
srand(9876543)  # set seed for repeatability
SPTF.new(6, 1.0, 5, 20.0).run
