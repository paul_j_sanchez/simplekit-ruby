SimpleKit for Ruby is a minimalist implementation of discrete event
simulation using event graphs.

Copyright (C) 2012-2020  Paul J. Sanchez

This library is free software; you can redistribute it and/or
modify it under the terms of the MIT free software license.

**This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.**

### DESCRIPTION

SimpleKit for Ruby is a very minimal Ruby implementation of discrete event
simulation based on Lee Schruben's "Event Graph" approach.  I wrote it to
illustrate to CS students how simulation works "under the hood".  You are
welcome to use it as you wish so long as you clearly understand that it was
created to be a teaching tool.  For serious simulation modeling with event
graphs I recommend Lee Schruben's `SIGMA` package or Arnie Buss's `SimKit`.

### WHAT'S WHAT

This top level folder contains this file, `README.md`, and the
`simplekit.gemspec` used to create the gem installation.

To install `SimpleKit` for Ruby, use Ruby's `gem` command:

	gem build simplekit.gemspec
	gem install simplekit-<version>.gem

(You may need to authenticate as an administrator to install gems.)

There are two subdirectories.  Source code for `SimpleKit` can be found
in `lib/simplekit.rb`.  Local documentation can be generated using Ruby's
`rdoc` command or `yard`.

Examples can be found in the `demos` folder, which contains several relatively simple models to
illustrate `SimpleKit` usage.  These include samples of
event scheduling, both with and without arguments, and event cancellation.
