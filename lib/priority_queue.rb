# Array-based PriorityQueue implementation
class PriorityQueue

  def initialize(array = [], ordering: :min, &blk)
    if block_given?
      @cmp = blk
      extend ClosureHeap
    else
      case ordering
        when :min
          extend MinHeap
        when :max
          extend MaxHeap
        else
          fail "Invalid ordering specified for PriorityQueue"
      end
    end
    @element = [nil] + array
    (@element.size / 2).downto(1) { |i| bubble_down i } if @element.size > 2
  end

  # Push +element+ onto the priority queue.
  def <<(new_element)
    @element << new_element
    bubble_up(@element.size - 1)
  end

  alias push <<

  # Replace the root without popping, return old root's value
  def replace_first(new_element)
    old_root = @element[1]
    @element[1] = new_element
    bubble_down(1)
    old_root
  end

  # Inspect the element at the head of the queue.
  def peek
    @element[1]
  end

  # Remove and return the next element from the queue, determined by priority.
  def pop
    return nil if @element.size < 2
    first = @element[1]
    last_elt = @element.pop
    replace_first(last_elt) if @element.size > 1
    first
  end

  # Reset the priority queue to empty.
  def clear
    @element = [nil]
  end

  # Return a boolean indicating whether the queue is empty or not
  def empty?
    @element.size < 2
  end

  # Return the number of elements stored in the queue
  def size
    @element.size - 1
  end

  private

  module MinHeap
    def bubble_up(index)
      target = @element[index]
      while index > 1
        parent_index = index / 2
        break if @element[parent_index] <= target
        @element[index] = @element[parent_index]
        index = parent_index
      end
      @element[index] = target
    end

    def bubble_down(index)
      target = @element[index]
      while true
        child_index = index * 2
        break if child_index >= @element.size
        if child_index < @element.size - 1
          right_index = child_index + 1
          child_index = right_index if @element[right_index] < @element[child_index]
        end
        break if target <= @element[child_index]
        @element[index] = @element[child_index]
        index = child_index
      end
      @element[index] = target
    end
  end

  module MaxHeap
    def bubble_up(index)
      target = @element[index]
      while index > 1
        parent_index = index / 2
        break if @element[parent_index] >= target
        @element[index] = @element[parent_index]
        index = parent_index
      end
      @element[index] = target
    end

    def bubble_down(index)
      target = @element[index]
      while true
        child_index = index * 2
        break if child_index >= @element.size
        if child_index < @element.size - 1
          right_index = child_index + 1
          child_index = right_index if @element[right_index] > @element[child_index]
        end
        break if target >= @element[child_index]
        @element[index] = @element[child_index]
        index = child_index
      end
      @element[index] = target
    end
  end

  module ClosureHeap
    def bubble_up(index)
      target = @element[index]
      while index > 1
        parent_index = index / 2
        break if @cmp.call(@element[parent_index], target)
        @element[index] = @element[parent_index]
        index = parent_index
      end
      @element[index] = target
    end

    def bubble_down(index)
      target = @element[index]
      while true
        child_index = index * 2
        break if child_index >= @element.size
        if child_index < @element.size - 1
          right_index = child_index + 1
          child_index = right_index if @cmp.call(@element[right_index], @element[child_index])
        end
        break if @cmp.call(target, @element[child_index])
        @element[index] = @element[child_index]
        index = child_index
      end
      @element[index] = target
    end
  end

end
