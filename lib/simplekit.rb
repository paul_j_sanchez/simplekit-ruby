require 'set'
require_relative 'priority_queue'

# The +SimpleKit+ module provides basic event scheduling capabilities.
#
# Including +SimpleKit+ in your simulation model gives you methods +:run+,
# +:model_time+, +:schedule+, +:cancel+, +:cancel_all+, and +:halt+ as mixins.
# <b>DO NOT</b> create your own implementations of methods with these names
# in your model. All but +:run+ are delegated to the +EventScheduler+ class.
module SimpleKit
  # The set of module methods to be passed to the EventScheduler
  # if not found in the model class.
  DELEGATED_METHODS = %i[
    model_time schedule cancel cancel_all halt
  ].freeze

  # Run your model by creating a new +EventScheduler+ and invoking its
  # +run+ method.
  def run
    @my_sim = EventScheduler.new(self)
    @my_sim.run
  end

  # If a method doesn't exist in the model class, try to delegate it
  # to +EventScheduler+.
  def method_missing(name, *args)
    DELEGATED_METHODS.include?(name) ? @my_sim.send(name, *args) : super
  end

  # Class +EventScheduler+ provides the computation engine for a
  # discrete event simulation model. It uses an array-based priority
  # queue implementation for the pending events list.
  #
  # Users must create a model class which:
  # * implements an +init+ method;
  # * Instantiates a model and invokes the +run+ method to start execution.
  class EventScheduler
    attr_reader :model_time, :user_model

    # Initialize the +EventScheduler+ by remembering the specified model
    # and setting up an empty event list.
    def initialize(the_model)
      @user_model = the_model
      @event_list = PriorityQueue.new
      @cancel_set = {}
    end

    # Add an event to the pending events list.
    #
    # *Arguments*::
    #   - +event+ -> the event to be scheduled.
    #   - +delay+ -> the amount of time which should elapse before
    #     the event executes.
    #   - +args+ -> zero or more named arguments to pass to the event
    #     at invocation time. These should be specified with labels, and
    #     consequently they can be placed in any order.
    def schedule(event, delay, args = {})
      raise 'Model scheduled event with negative delay.' if delay < 0
      @event_list.push EventNotice.new(event, @model_time, delay, args)
    end

    # Cancel an individual occurrence of event type +event+.
    # If no +args+ are provided, the next scheduled occurrence of +event+
    # is targeted.  If a subset of the event's +args+ is provided, they must
    # all be a match with the corresponding +args+ of the scheduled event
    # in order for the cancellation to apply, but +args+ which are not
    # specified do not affect the target event matching.
    def cancel(event, args = {})
      @cancel_set[event] ||= Set.new
      # @cancel_set[event].add(args.empty? ? nil : args)
      @cancel_set[event].add(args)
    end

    # Cancel all currently scheduled events of type +event+.
    def cancel_all(event)
      if event
        PriorityQueue.new.tap do |pq|
          while (event_notice = @event_list.pop)
            pq.push event_notice unless event_notice.event == event
          end
          @event_list = pq
        end
      end
    end

    # Start execution of a model. The simulation +model_time+ is initialized
    # to zero and the model is initialized via the mandatory +init+ method.
    # Then loop while events are pending on the +event_list+. The event with the
    # smallest time is popped, +model_time+ is updated to the event time, and
    # the event method is invoked with the +args+, if any, set by +schedule+.
    def run
      @model_time = 0.0
      @user_model.init
      while (current_event = @event_list.pop)
        next if should_cancel?(current_event)
        @model_time = current_event.time
        if current_event.args.empty?
          @user_model.send(current_event.event)
        else
          @user_model.send(current_event.event, **current_event.args)
        end
      end
    end

    # Clear the event list, which causes termination of the simulation.
    # Never schedule any new events after invoking +halt+.
    def halt
      @event_list.clear
    end

    private

    # Private method that returns a boolean to determine if +event_notice+
    # represents an event subject to cancellation.
    def should_cancel?(event_notice)
      e = event_notice.event
      if @cancel_set.key? e
        if @cancel_set[e].include? nil
          @cancel_set[e].delete nil
          @cancel_set.delete e if @cancel_set[e].empty?
          return true
        else
          for hsh in @cancel_set[e] do
            next unless event_notice.args >= hsh
            @cancel_set[e].delete hsh
            @cancel_set.delete e if @cancel_set[e].empty?
            return true
          end
        end
      end
      false
    end

    # This is a private helper class for the EventScheduler class.
    # Users should never try to access this directly.
    class EventNotice
      attr_reader :event, :time, :time_stamp, :priority, :args

      def initialize(event, time, delay, args)
        @event = event
        @time_stamp = time
        @time = time + delay
        @args = args
        @priority = @args && @args.key?(:priority) ? @args.delete(:priority) : 10
      end

      include Comparable
      # Compare EventNotice objects for ordering, first by time,
      # breaking ties using priority.
      def <=>(other)
        (time <=> other.time).tap do |outcome|
          return priority <=> other.priority if outcome == 0
        end
      end
    end
  end
end
