# -*- ruby -*-
_VERSION = "1.1.0"

Gem::Specification.new do |s|
  s.name = "simplekit"
  s.version = _VERSION
  s.date = "2020-04-09"
  s.summary = "Discrete event simulation engine."
  s.homepage = "https://gitlab.nps.edu/pjsanche/simplekit-ruby.git"
  s.email = "pjs@alum.mit.edu"
  s.description = "This is a minimal discrete event simulation scheduling algorithm for educational use."
  s.author = "Paul J Sanchez"
  s.files = %w[
    simplekit.gemspec
    lib/simplekit.rb
    lib/priority_queue.rb
    demos/MMk.rb
    demos/MMk_reference_output.txt
    demos/MMk_shutdown.rb
    demos/CancelQ.rb
    demos/sptf.rb
  ]
  s.required_ruby_version = '>= 2.3.0'
  s.license = 'MIT'
end
