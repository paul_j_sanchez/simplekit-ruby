#!/usr/bin/env ruby

require_relative '../lib/simplekit'

class MyModel
  include SimpleKit

  def init
    @x = 1
    schedule(:increment, 1.0)
    schedule(:halt, 11)
  end

  def increment
    @x += 1
    schedule(:increment, 1.5)
    printf "%f, %f\n", model_time, @x
  end
end

MyModel.new.run
