#!/usr/bin/env ruby

require_relative '../lib/simplekit'

class MyModel
  include SimpleKit

  def init
    @x = 1
    schedule(:increment, rand(2), n: 1, char: 'z'.ord)
    cancel(:increment, char: 'q'.ord)
  end

  def increment(n:, char:)
    @x += n
    printf "%f, %f, %c\n", model_time, @x, char
    schedule(:increment, 2.0 * rand(2), n: @x, char: char - 1, priority: 3)
  end
end

srand(42)
MyModel.new.run
