#!/usr/bin/env ruby

require_relative '../lib/simplekit'

class MyModel
  include SimpleKit

  def init
    @x = 1
    schedule(:increment, rand(2), n: 1, char: 'z'.ord)
  end

  def increment(n:, char:)
    if model_time < 10.0
      @x += n
      schedule(:increment, rand(3), n: @x, char: char - 1, priority: 3)
      printf "%f, %f, %c\n", model_time, @x, char
    else
      cancel_all :increment
    end
  end
end

srand(42)
MyModel.new.run
