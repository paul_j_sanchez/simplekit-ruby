#!/usr/bin/env ruby

require_relative '../lib/simplekit'

# Demonstration model of an M/M/k queueing system.  There are k servers
# and both the arrival and service processes are memoryless (exponential).
class MMk
  include SimpleKit

  # Constructor - initializes the model parameters.
  # param: arrival_rate - The rate at which customers arrive to the system.
  # param: service_rate - The rate at which individual servers serve.
  # param: max_servers - The total number of servers in the system.
  def initialize(arrival_rate, service_rate, max_servers)
    @arrival_rate = arrival_rate
    @service_rate = service_rate
    @max_servers = max_servers
  end

  # Initialize the model state and schedule any necessary events.
  # Note that this particular model will terminate based on
  # time by scheduling a halt 100 time units in the future.
  def init
    @num_available_servers = @max_servers
    @q_length = 0
    schedule(:arrival, 0.0)
    schedule(:close_doors, 100.0)
    dump_state('init')
  end

  # An arrival event increments the queue length, schedules the next
  # arrival, and schedules a begin_service event if a server is available.
  def arrival
    @q_length += 1
    schedule(:arrival, exponential(@arrival_rate))
    schedule(:begin_service, 0.0) if @num_available_servers > 0
    dump_state('arrival')
  end

  # Start service for the first customer in line, removing that
  # customer from the queue and utilizing one of the available servers.
  # An end_service will be scheduled.
  def begin_service
    @q_length -= 1
    @num_available_servers -= 1
    schedule(:end_service, exponential(@service_rate))
    dump_state('begin svc')
  end

  # Frees up an available server, and schedules a begin_service if
  # anybody is waiting in line.
  def end_service
    @num_available_servers += 1
    schedule(:begin_service, 0.0) if @q_length > 0
    dump_state('end svc')
  end

  def close_doors
    cancel(:arrival)
  end

  # Exponential random variate generator.
  # param: rate - The rate (= 1 / mean) of the distribution.
  # returns: A realization of the specified distribution.
  def exponential(rate)
    -Math.log(rand) / rate
  end

  # A report mechanism which dumps the time, current event, and values
  # of the state variables to the console.
  # param: event - The name of the event which invoked this method.
  def dump_state(event)
    printf "Time: %8.3f\t%10s - Q: %d\tServers Available: %d\n",
           model_time, event, @q_length, @num_available_servers
  end
end

# Instantiate an MMk object with a particular parameterization and run it.
srand 7_654_321
MMk.new(4.5, 1.0, 5).run
